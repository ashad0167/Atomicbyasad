<?php


namespace App\ProfilPicture;


use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class ProfilPicture extends DB
{
    private $id;
    private $name;
    private $image;
    private $soft_deleted;


    public function setData($postData)
    {
        if (array_key_exists("id", $postData)) {
            $this->id = $postData['id'];
        }
        if (array_key_exists("name", $postData)) {
            $this->name = $postData['name'];
        }
        if (array_key_exists("image", $postData)) {
            $this->image = $postData['image'];
        }
        if (array_key_exists("soft_deleted", $postData)) {
            $this->soft_deleted = $postData['soft_deleted'];
        }
    }


    public function store()
    {
        $dataArray = array($this->name, $this->image);

        $sql = "Insert into profil_picture(name,image) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success! data has been inserted successfully");
        } else {
            Message::message("failed! data has not been inserted.");
        }


        Utility::redirect("create.php");

    }

    public function index()
    {

        $sql = "Select * from profil_picture WHERE soft_deleted='No'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }


    public function view()
    {

        $sql = "Select * from profil_picture WHERE id=" . $this->id;

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }


    public function trashed()
    {

        $sql = "Select * from profil_picture WHERE soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

    }


    public function update()
    {
        $dataArray = array($this->name, $this->image);

        $sql = "UPDATE  profil_picture SET name=?, image=? where id=" . $this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success! data has been Updated successfully");
        } else {
            Message::message("Failed! data has not been Updated.");
        }


        Utility::redirect("index.php");     // This change Not included in Class Video

    }


    public function trash()
    {
        $dataArray = array("Yes");

        $sql = "UPDATE profil_picture SET soft_deleted = ? where id=" . $this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success! data has been Soft Deleted successfully");
        } else {
            Message::message("Failed! data has not been Soft Deleted.");
        }


        Utility::redirect("trashed.php");     // This change Not included in Class Video
    }


    public function recover()
    {

        $dataArray = array("No");

        $sql = "UPDATE  profil_picture SET soft_deleted = ? where id=" . $this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("Success! data has been Recovered successfully");
        } else {
            Message::message("Failed! data has not been Recovered.");
        }


        Utility::redirect("index.php");     // This change Not included in Class Video

    }

    public function delete()
    {

        $sql = "DELETE from profil_picture  where id=" . $this->id;

        $result = $this->DBH->exec($sql);


        if ($result) {
            Message::message("Success! data has been Deleted successfully");
        } else {
            Message::message("Failed! data has not been Deleted.");
        }


        Utility::redirect("index.php");

    }


    public function indexPaginator($page = 1, $itemsPerPage = 3)
    {
        try {

            $start = (($page - 1) * $itemsPerPage);
            if ($start < 0) $start = 0;
            $sql = "SELECT * from profil_picture  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";


        } catch (PDOException $error) {

            $sql = "SELECT * from profil_picture  WHERE soft_deleted = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();
        return $arrSomeData;


    }


    public function trashedPaginator($page = 1, $itemsPerPage = 3)
    {

        try {

            $start = (($page - 1) * $itemsPerPage);
            if ($start < 0) $start = 0;
            $sql = "SELECT * from profil_picture  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";


        } catch (PDOException $error) {

            $sql = "SELECT * from profil_picture  WHERE soft_deleted = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();
        return $arrSomeData;


    }


    public function trashMultiple($selectedIDsArray)
    {


        foreach ($selectedIDsArray as $id) {

            $sql = "UPDATE  profil_picture SET soft_deleted='Yes' WHERE id=" . $id;

            $result = $this->DBH->exec($sql);

            if (!$result) break;

        }


        if ($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray)
    {


        foreach ($markArray as $id) {

            $sql = "UPDATE  profil_picture SET soft_deleted='No' WHERE id=" . $id;

            $result = $this->DBH->exec($sql);

            if (!$result) break;

        }


        if ($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }


    public function deleteMultiple($selectedIDsArray)
    {


        foreach ($selectedIDsArray as $id) {

            $sql = "Delete from profil_picture  WHERE id=" . $id;

            $result = $this->DBH->exec($sql);

            if (!$result) break;

        }


        if ($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }


    public function listSelectedData($selectedIDs)
    {


        foreach ($selectedIDs as $id) {

            $sql = "Select * from profil_picture  WHERE id=" . $id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[] = $STH->fetch();


        }


        return $someData;


    }


    public function search($requestArray)
    {
        $sql = "";
        if (isset($requestArray['byName']) && isset($requestArray['byAuthor']))
            $sql = "SELECT * FROM `profil_picture` WHERE `soft_deleted` ='No'
AND (`name` LIKE '%" . $requestArray['search'] . "%' OR `image` LIKE '%" . $requestArray['search'] . "%')";

        if (isset($requestArray['byName']) && !isset($requestArray['byAuthor']))
            $sql = "SELECT * FROM `profil_picture` WHERE `soft_deleted` ='No'
AND `name` LIKE '%" . $requestArray['search'] . "%'";
        if (!isset($requestArray['byName']) && isset($requestArray['byAuthor']))
            $sql = "SELECT * FROM `profil_picture` WHERE `soft_deleted` ='No'
 AND `image` LIKE '%" . $requestArray['search'] . "%'";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString = strip_tags($oneData->name);
            $eachString = trim($eachString);
            $eachString = preg_replace("/\r|\n/", " ", $eachString);
            $eachString = str_replace("&nbsp;", "", $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord) {
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->image);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString = strip_tags($oneData->image);
            $eachString = trim($eachString);
            $eachString = preg_replace("/\r|\n/", " ", $eachString);
            $eachString = str_replace("&nbsp;", "", $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord) {
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }
}