<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/9/2017
 * Time: 3:28 PM
 */

namespace App\SummaryOfCompany;


use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
use PDOException;


class Summary extends Database
{

    private $id;
    private $name;
    private $summary;
    private $soft_deleted;


    public function setData($arrayData)
    {
        if (array_key_exists("id", $arrayData))
            $this->id = $arrayData['id'];

        if (array_key_exists("Name", $arrayData))
            $this->name = $arrayData['Name'];

        if (array_key_exists("Summary", $arrayData))
            $this->summary = $arrayData['Summary'];

        if (array_key_exists("soft_deleted", $arrayData))
            $this->soft_deleted = $arrayData['soft_deleted'];


    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    public function StoreToDataBase()
    {
        $arrData = array($this->name, $this->summary);

        $sql = "INSERT into summary_of_organization (name,summary) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('indexData.php');

    }

    public function GetAllRecords()
    {

        $sql = "select * from summary_of_organization where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function storeDt()
    {

        $arrData = array($this->name, $this->summary);

        $sql = "INSERT into  summary_of_organization (name,summary) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('ViewAllCompanyData.php');

    }

    public function indexDt()
    {
        $sql = "select * from summary_of_organization where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();
    }


    public function indexPaginatorDt($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from summary_of_organization WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from summary_of_organization WHERE soft_deleted = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function deleteDt()
    {

        $sql = "Delete from summary_of_organization WHERE id=" . $this->id;

        $result = $this->DBH->exec($sql);


        if ($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('indexData.php');


    }

    public function viewDt(){

        $sql = "select * from summary_of_organization where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }

    public function trashedDt()
    {

        $sql = "select * from summary_of_organization where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function softDeleted(){
        $dataArray = array("Yes");
        $SQL = "UPDATE summary_of_organization SET soft_deleted=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Soft Deleted");
        else
            Message::message("Soft Deleted Failed");

        Utility::redirect("ViewAllCompanyData.php");
    }

    public function trashedPaginatorDt($page = 1, $itemsPerPage = 3)
    {

        try {

            $start = (($page - 1) * $itemsPerPage);
            if ($start < 0) $start = 0;
            $sql = "SELECT * from summary_of_organization  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";


        } catch (PDOException $error) {

            $sql = "SELECT * from summary_of_organization  WHERE soft_deleted = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();
        return $arrSomeData;


    }

    public function trashDt()
    {

        $sql = "UPDATE  summary_of_organization SET soft_deleted='Yes' WHERE id=" . $this->id;

        $result = $this->DBH->exec($sql);


        if ($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('indexData.php');


    }

    public function trashMultipleDt($selectedIDsArray)
    {


        foreach ($selectedIDsArray as $id) {

            $sql = "UPDATE   summary_of_organization  SET soft_deleted='Yes' WHERE id=" . $id;

            $result = $this->DBH->exec($sql);

            if (!$result) break;

        }

    }


    public function deleteMultipleDt($selectedIDsArray)
    {


        foreach ($selectedIDsArray as $id) {

            $sql = "Delete from summary_of_organization   WHERE id=" . $id;

            $result = $this->DBH->exec($sql);

            if (!$result) break;

        }


        if ($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('indexData.php?Page=1');


    }


    public function listSelectedData($selectedIDs)
    {


        foreach ($selectedIDs as $id) {

            $sql = "Select * from summary_of_organization  WHERE id=" . $id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[] = $STH->fetch();


        }


        return $someData;


    }

    public function update()
    {

        $arrData = array($this->name, $this->summary);

        $sql = "UPDATE  summary_of_organization SET name=?,summary=? WHERE id=" . $this->id;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('indexData.php');


    }


    public function recover()
    {

        $sql = "UPDATE  summary_of_organization SET soft_deleted='No' WHERE id=" . $this->id;

        $result = $this->DBH->exec($sql);


        if ($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");


        Utility::redirect('indexData.php');


    }


    public function recoverMultipleDt($markArray)
    {


        foreach ($markArray as $id) {

            $sql = "UPDATE summary_of_organization SET soft_deleted='No' WHERE id=" . $id;

            $result = $this->DBH->exec($sql);

            if (!$result) break;

        }


    }

    public function searchDt($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['bySummary']) )  $sql = "SELECT * FROM `summary_of_organization` WHERE `soft_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `summary` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['bySummary']) ) $sql = "SELECT * FROM `summary_of_organization` WHERE `soft_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['bySummary']) )  $sql = "SELECT * FROM `summary_of_organization` WHERE `soft_deleted` ='No' AND `summary` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()



    public function setDataDt($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('summary',$postData)){
            $this->summary = $postData['summary'];
        }

    }


    public function getAllKeywordsDt()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->indexDt();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->indexDt();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->indexDt();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->summary);
        }
        $allData = $this->indexDt();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->summary);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords





}
