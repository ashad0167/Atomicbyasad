<?php

require_once ("../../../vendor/autoload.php");

use \App\AddGender\AddGender;

$objAddGender = new AddGender();
$objAddGender->setData($_POST);
$objAddGender->update();
