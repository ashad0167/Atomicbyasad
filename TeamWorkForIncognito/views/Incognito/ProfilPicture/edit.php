<?php

require_once ('../../../vendor/autoload.php');

use App\ProfilPicture\ProfilPicture;
$objProfile = new ProfilPicture();
$objProfile->setData($_GET);
$oneData = $objProfile->view();


$ext = strtolower(end(explode('.',$oneData->image)));

if($ext){
    $image = "<img style='width: 100px;height: 100px'src='Upload/$oneData->image' id='showProfile'>";
    $msg = "Your Current Profile Picture";
}else{
    $image = "<img style='width: 100px;height: 100px'src='../../../images/default.jpg' id='showProfile'>";
    $msg = "Your did'nt Upload Any Profile Picture";
}

?>




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile Picture-Edit</title>


    <link href="../../../resource/style.css" rel="stylesheet">
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>
        body{
            background: url("profile2.jpg") no-repeat;
            background-size: 100%;

        }

        .information{

            background-color:#1b6d85;
            color: #fff;
            font-weight: bold;
            padding: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            align-content: center;
            align-items: center;
            alignment: center;

            border: solid;


        }

        .main{
            align-content: center;
            align-items: center;
            alignment: center;
            width:500px;
            height: 300px;
            display: inline-block;


        }

        h1{
            color: #1b6d85;
        }
    </style>


</head>
<body>
<div class="container">

    <div class="navbar">
        <td><a href='../../../../index.html' class='btn btn-group-lg btn-info'>Home</a> </td>
        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>

    <center>
        <div class="main">



            <div class="col-lg-offset-2 col-lg-9" style="padding: 10px;border-radius: 5px; background-color: #1b6d85">
                <div class="col-lg-offset-0 col-lg-4"><?php echo $image;?></div>
                <span class="col-lg-offset-0 col-lg-8" style="display: block; margin-top: 25px; font-size: 17px; color: white"><?php echo $msg;?></span>
            </div>


            <h1>Edit Profile Picture</h1><br>
            <div class="information">



                <form  class="form-group f" action="update.php" method="post" enctype="multipart/form-data">



                    <h3>  Name:</h3>
                    <input  type="text" class="form-control" name="name" id="input" placeholder="Enter Your Name" value="<?php echo $oneData->name;?>">



                    <br>
                    <h3>Photo:</h3>
                            <input type="file" name="image" id="exampleInputFile">






                <input type="hidden" name="id" value="<?php echo $oneData->id?>">


                    <br>
                    <br>
                    <input type="submit" value="Upload"class='btn btn-group-lg btn-info'>



            </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>