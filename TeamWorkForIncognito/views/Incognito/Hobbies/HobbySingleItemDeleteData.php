<?php
require_once("../../../vendor/autoload.php");

$hobby = new \App\Hobby\Hobbies();
$hobby->setData($_GET);
$oneData = $hobby->getSingleData();

use App\Message\Message;

if(isset($_GET['Yes']) && $_GET['Yes']==1){
    $hobby->singleItemDelete();
    $_GET['Yes'] = 0;

}

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Delete ?</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("hobby1.jpg") no-repeat;
            background-size: 100%;

        }
        h1{
            color: white;
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center";">Are You Sure You Want To Parmanently Delete The Following Record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>Hobby</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->hobby</td>
                  </tr>
              ";

        ?>

    </table>

    <a href='HobbySingleItemDeleteData.php?id=<?php echo $oneData->id?>&Yes=1' class='btn btn-group-lg btn-info'>Yes</a>
    <a href='ViewAllHobbyData.php' class='btn btn-group-lg btn-info'>No</a>





</div>

</body>
</html>