<?php
require_once("../../../vendor/autoload.php");

use \App\BookTitle\BookTitle;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$email= new \App\Email\Email();


$email->multipleSelectedItemTrashed($_POST['mark']);
    Utility::redirect("viewAllsoftDeletedFromEmail.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("ViewAllData.php");
}