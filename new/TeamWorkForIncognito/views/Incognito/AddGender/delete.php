<?php
require_once("../../../vendor/autoload.php");

$objAddGender = new \App\AddGender\AddGender();
$objAddGender->setData($_GET);
$oneData = $objAddGender->view();

if(isset($_GET['YesButton'])) $objAddGender->delete();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Gender - Single Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("gender2.jpg") no-repeat;
            background-size: 100%;

        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>AddGender's Name</th>
            <th>Gender</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->gender</td>

                  </tr>
              ";

        ?>

    </table>

    <?php
    echo "
          <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='index.php' class='btn btn-success'>No</a>
        ";

    ?>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>