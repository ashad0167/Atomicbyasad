<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/9/2017
 * Time: 3:27 PM
 */

namespace App\Hobby;


use App\Model\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO,PDOException;

class Hobbies extends Database
{
    private $id;
    private $name;
    private $hobbies;
    private $soft_deleted;

    public function setData($arrayDtata){
        if(array_key_exists("id",$arrayDtata))
            $this->id = $arrayDtata['id'];
        if(array_key_exists("hobbies",$arrayDtata))
            $this->hobbies = $arrayDtata['hobbies'];
        if(array_key_exists("personName",$arrayDtata))
            $this->name = $arrayDtata['personName'];
        if(array_key_exists("soft_deleted",$arrayDtata))
            $this->soft_deleted = $arrayDtata['soft_deleted'];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getHobbies()
    {
        return $this->hobbies;
    }

    public function StoreToDataBase(){
        $arrData = array($this->name,$this->hobbies);

        $sql = "INSERT into hobby_table(name,hobby) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('ViewAllHobbyData.php');
    }

    public function getAllRecords(){

        $sql = "select * from hobby_table where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function getSingleData(){

        $sql = "select * from hobby_table where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }

    public function editRecords(){
        $dataArray = array($this->name,$this->hobbies);
        $SQL = "UPDATE hobby_table SET name=?, hobby=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Updated");
        else
            Message::message("Update Failed");

        Utility::redirect("ViewAllHobbyData.php");
    }

    public function getAllsoftDeletedDataRecords(){
        $SQL = "Select * from hobby_table WHERE soft_deleted='Yes'";
        $statementHandler = $this->DBH->query($SQL);
        $statementHandler->setFetchMode(PDO::FETCH_OBJ);
        return $statementHandler->fetchAll();
    }


    public function softDeleted(){
        $dataArray = array("Yes");
        $SQL = "UPDATE hobby_table SET soft_deleted=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Soft Deleted");
        else
            Message::message("Soft Deleted Failed");

        Utility::redirect("ViewAllHobbyData.php");
    }

    public function recovery(){
        $dataArray = array("No");
        $SQL = "UPDATE hobby_table SET soft_deleted=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Recovered");
        else
            Message::message("Soft Recovered Failed");

        Utility::redirect("ViewAllHobbyData.php");
    }
    public function recoverMultiple($markArray){

        foreach($markArray as $id){

            $sql = "UPDATE  hobby_table SET soft_deleted='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }

        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");

        Utility::redirect("ViewAllHobbyData.php?Page=1");
    }

    public function singleItemDelete(){
        $SQL = "DELETE from hobby_table WHERE id=".$this->id;
        $result = $this->DBH->exec($SQL);
        if($result)
            Message::message("Recorde Deleted");
        else
            Message::message("Failed, Records Not Deleted");

        Utility::redirect("ViewAllHobbyData.php");
    }

    public function hobbyAllrecordsPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from hobby_table  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        }catch (PDOException $error){

            $sql = "SELECT * from hobby_table  WHERE soft_deleted = 'No'";

        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }

    public function hobbyAllSoftDeletedDataPaginator($page=1,$itemsPerPage=3){
        $start = (($page-1) * $itemsPerPage);
        $sql = "SELECT * from hobby_table  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }

    public function hobbyItemsearch($requestArray){
        $sql = "";

        if( isset($requestArray['byName']) && isset($requestArray['byHobby']) )  $sql = "SELECT * FROM `hobby_table` WHERE `soft_deleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `hobby` LIKE '%".$requestArray['search']."%')";

        if(isset($requestArray['byName']) && !isset($requestArray['byHobby']) ) $sql = "SELECT * FROM `hobby_table` WHERE `soft_deleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";

        if(!isset($requestArray['byName']) && isset($requestArray['byHobby']) )  $sql = "SELECT * FROM `hobby_table` WHERE `soft_deleted` ='No' AND `hobby` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();
        return $someData;
    }// end of search()

    public function getAllKeywordsfroHoobby(){
        $_allKeywords = array();
        //$WordsArr = array();

        $allData = $this->getAllRecords();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        // $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each  Name search field block end

        // for each Email search field block start
        $allData = $this->getAllRecords();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->hobby);
        }
        $allData = $this->getAllRecords();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->hobby);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each Email search field block end

        return array_unique($_allKeywords);

    }// get all keywords

    public function listSelectedData($selectedIDs){
        foreach($selectedIDs as $id){
            $sql = "Select * from hobby_table  WHERE id=".$id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData[]  = $STH->fetch();
        }
        return $someData;
    }

    public function deleteMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){
            $sql = "Delete from hobby_table  WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");
        Utility::redirect('ViewAllHobbyData.php?Page=1');
    }

    public function multipleSelectedItemTrashed($selectedIDsArray){
        foreach($selectedIDsArray as $id){
            $sql = "UPDATE  hobby_table SET soft_deleted='Yes' WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }

        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('ViewAllSoftDeletedData.php?Page=1');
    }

}